<!DOCTYPE html>
</html>
<head>
	<title></title>
</head>
<body>
<pre>
<?php
// Написать рекурсивную процедуру перевода натурального числа из десятичной системы счисления в N-ричную. Значение N в основной программе вводится с клавиатуры (2 N 16).
//5 
//2 1
//1 0
//0 1 
//0 0 


function perevod($n,$e=3){
	$number = $n % $e;
	$n = ($n-$number)/$e;
		
	if($n!=0 || $number!=0){
		perevod($n);
		switch($number){
			case 10: echo 'A';break;
			case 11: echo 'B';break;
			case 12: echo 'C';break;
			case 13: echo 'D';break;
			case 14: echo 'E';break;
			case 15: echo 'F';break;
			default :echo $number; 
		}
			
	}
}
$e = 2;

perevod(255);

?>
</body>
</html>

